# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-24 20:21-0700\n"
"PO-Revision-Date: 2021-04-15 11:27+0000\n"
"Last-Translator: J. Lavoie <j.lavoie@net-c.ca>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/gnu-mailman/"
"django-mailman3/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6-dev\n"

#: forms.py:32
msgid "Username"
msgstr ""

#: forms.py:33
msgid "First name"
msgstr ""

#: forms.py:34
msgid "Last name"
msgstr ""

#: forms.py:36
msgid "Time zone"
msgstr ""

#: forms.py:43
msgid "A user with that username already exists."
msgstr "Ya existe otro usuario con ese nombre de usuario."

#: templates/account/email.html:6
#: templates/django_mailman3/profile/base.html:17
msgid "Account"
msgstr "Cuenta"

#: templates/account/email.html:11
msgid "The following e-mail addresses are associated with your account:"
msgstr "Las siguientes direcciones de correo están asociadas a su cuenta:"

#: templates/account/email.html:25
msgid "Verified"
msgstr "Verificada"

#: templates/account/email.html:27
msgid "Unverified"
msgstr "No verificada"

#: templates/account/email.html:29
msgid "Primary"
msgstr "Primaria"

#: templates/account/email.html:35
msgid "Make Primary"
msgstr "Seleccionar como primaria"

#: templates/account/email.html:36
msgid "Re-send Verification"
msgstr "Reenviar verificación"

#: templates/account/email.html:37 templates/socialaccount/connections.html:34
msgid "Remove"
msgstr "Quitar"

#: templates/account/email.html:44
msgid "Warning:"
msgstr "Atención:"

#: templates/account/email.html:44
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, etc."
msgstr ""
"No tiene ninguna dirección de correo configurada. Es importante que lo haga "
"para poder recibir notificaciones, recuperar su contraseña, etc."

#: templates/account/email.html:49
msgid "Add E-mail Address"
msgstr "Agregar dirección de correo"

#: templates/account/email.html:55
msgid "Add E-mail"
msgstr "Añadir un correo electrónico"

#: templates/account/email.html:66
msgid "Do you really want to remove the selected e-mail address?"
msgstr "¿Está seguro de que quiere borrar la dirección seleccionada?"

#: templates/account/email_confirm.html:6
#: templates/account/email_confirm.html:10
msgid "Confirm E-mail Address"
msgstr "Confirmar dirección de correo"

#: templates/account/email_confirm.html:16
#, python-format
msgid ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."
msgstr ""
"Por favor confirme que <a href=\"mailto:%(email)s\">%(email)s</a> es una "
"dirección de correo electrónico del usuario %(user_display)s."

#: templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "Confirmar"

#: templates/account/email_confirm.html:27
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please <a href="
"\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr ""
"Este enlace de confirmación de corre electrónico ha expirado o no es válido. "
"Por favor <a href=\"%(email_url)s\">Solicite una nueva confirmación de "
"correo electrónico</a>."

#: templates/account/login.html:7 templates/account/login.html:11
#: templates/account/login.html:48
msgid "Sign In"
msgstr "Registrarse"

#: templates/account/login.html:18
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign "
"up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"Por favor ingrese con una\n"
"de sus cuentas de terceros. O, <a href=\"%(signup_url)s\">regístrese</a>\n"
"en %(site_name)s y luego ingrese abajo:"

#: templates/account/login.html:30
#: templates/django_mailman3/profile/profile.html:72
msgid "or"
msgstr "o"

#: templates/account/login.html:37
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"Si todavía no ha creado una cuenta, por favor \n"
"<a href=\"%(signup_url)s\">regístrese</a> antes."

#: templates/account/login.html:50
msgid "Forgot Password?"
msgstr "¿Olvidó su clave?"

#: templates/account/logout.html:5 templates/account/logout.html:8
#: templates/account/logout.html:17
msgid "Sign Out"
msgstr "Salir"

#: templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "¿Está seguro de que quiere salir?"

#: templates/account/password_change.html:12
#: templates/account/password_reset_from_key.html:6
#: templates/account/password_reset_from_key.html:9
#: templates/django_mailman3/profile/base.html:20
msgid "Change Password"
msgstr "Cambiar contraseña"

#: templates/account/password_reset.html:7
#: templates/account/password_reset.html:11
msgid "Password Reset"
msgstr "Restablecimiento de contraseña"

#: templates/account/password_reset.html:16
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you "
"an e-mail allowing you to reset it."
msgstr ""
"¿Olvidó su contraseña? Ingrese su dirección de correo y le enviaremos un "
"correo electrónico con instrucciones para restablecerla."

#: templates/account/password_reset.html:22
msgid "Reset My Password"
msgstr "Restablecer mi contraseña"

#: templates/account/password_reset.html:27
msgid "Please contact us if you have any trouble resetting your password."
msgstr "Contáctenos si tiene algún problema restableciendo su contraseña."

#: templates/account/password_reset_from_key.html:9
msgid "Bad Token"
msgstr "Identificador no válido"

#: templates/account/password_reset_from_key.html:13
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</"
"a>."
msgstr ""
"El enlace para restablecer la contraseña no era válido, es posible que ya se "
"hubiera usado.   Por favor,  solicite un<a href=\"%(passwd_reset_url)s"
"\">nuevo restablecimiento de contraseña</a>."

#: templates/account/password_reset_from_key.html:20
msgid "change password"
msgstr "cambiar contraseña"

#: templates/account/password_reset_from_key.html:25
msgid "Your password is now changed."
msgstr "Se ha cambiado su contraseña."

#: templates/account/password_set.html:12
msgid "Set Password"
msgstr "Camiar contraseña"

#: templates/account/signup.html:6 templates/socialaccount/signup.html:6
msgid "Signup"
msgstr "Suscribirse"

#: templates/account/signup.html:9 templates/account/signup.html:20
#: templates/socialaccount/signup.html:9 templates/socialaccount/signup.html:21
msgid "Sign Up"
msgstr "Inscribirse"

#: templates/account/signup.html:11
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr ""
"¿Ya tiene cuenta? En ese caso, por favor, <a href=\"%(login_url)s\">acceda</"
"a>."

#: templates/django_mailman3/paginator/pagination.html:43
msgid "Jump to page:"
msgstr "Ir a la página:"

#: templates/django_mailman3/paginator/pagination.html:61
msgid "Results per page:"
msgstr "Resultados por página:"

#: templates/django_mailman3/paginator/pagination.html:77
#: templates/django_mailman3/profile/profile.html:71
msgid "Update"
msgstr "Actualizar"

#: templates/django_mailman3/profile/base.html:6
msgid "User Profile"
msgstr "Perfil de usuario"

#: templates/django_mailman3/profile/base.html:13
msgid "User profile"
msgstr "Perfil de usuario"

#: templates/django_mailman3/profile/base.html:13
msgid "for"
msgstr "para"

#: templates/django_mailman3/profile/base.html:23
msgid "E-mail Addresses"
msgstr "Direcciones de correo-e"

#: templates/django_mailman3/profile/base.html:30
msgid "Account Connections"
msgstr "Conexiones de la cuenta"

#: templates/django_mailman3/profile/base.html:35
#: templates/django_mailman3/profile/delete_profile.html:17
msgid "Delete Account"
msgstr "Eliminar cuenta"

#: templates/django_mailman3/profile/delete_profile.html:12
msgid ""
"Are you sure you want to delete your account? This will remove your account "
"along with all your subscriptions."
msgstr ""
"¿Está seguro que quiere eliminar la cuenta? Esto eliminará su cuenta junto "
"con todas sus suscripciones."

#: templates/django_mailman3/profile/profile.html:20
#: templates/django_mailman3/profile/profile.html:57
msgid "Edit on"
msgstr "Editar en"

#: templates/django_mailman3/profile/profile.html:28
msgid "Primary email:"
msgstr "Dirección principal:"

#: templates/django_mailman3/profile/profile.html:34
msgid "Other emails:"
msgstr "Otras direcciones:"

#: templates/django_mailman3/profile/profile.html:40
msgid "(no other email)"
msgstr "(no hay otras direcciones)"

#: templates/django_mailman3/profile/profile.html:45
msgid "Link another address"
msgstr "Añadir otra dirección"

#: templates/django_mailman3/profile/profile.html:53
msgid "Avatar:"
msgstr "Avatar:"

#: templates/django_mailman3/profile/profile.html:63
msgid "Joined on:"
msgstr "Suscrito el:"

#: templates/django_mailman3/profile/profile.html:72
msgid "cancel"
msgstr "cancelar"

#: templates/openid/login.html:10
msgid "OpenID Sign In"
msgstr "Acceder con OpenID"

#: templates/socialaccount/connections.html:9
msgid ""
"You can sign in to your account using any of the following third party "
"accounts:"
msgstr ""
"Puede acceder a su cuenta usando cualquiera de las siguientes cuentas de "
"otros sitios:"

#: templates/socialaccount/connections.html:42
msgid ""
"You currently have no social network accounts connected to this account."
msgstr ""
"En este momento no tiene cuentas de redes sociales conectadas a esta cuenta."

#: templates/socialaccount/connections.html:45
msgid "Add a 3rd Party Account"
msgstr "Añadir una cuenta de otro sitio"

#: templates/socialaccount/signup.html:11
#, python-format
msgid ""
"You are about to use your %(provider_name)s account to login to\n"
"%(site_name)s. As a final step, please complete the following form:"
msgstr ""
"Va a usar su cuenta de %(provider_name)s para acceder a%(site_name)s.\n"
"Como último paso, por favor, rellene el formulario siguiente:"

#: templatetags/pagination.py:43
msgid "Newer"
msgstr "Más reciente"

#: templatetags/pagination.py:44
msgid "Older"
msgstr "Más antiguo"

#: templatetags/pagination.py:46
msgid "Previous"
msgstr "Anterior"

#: templatetags/pagination.py:47
msgid "Next"
msgstr "Siguiente"

#: views/profile.py:72
msgid "The profile was successfully updated."
msgstr "El perfil se ha actualizado con éxito."

#: views/profile.py:74
msgid "No change detected."
msgstr "No se han detectado cambios."

#: views/profile.py:110
msgid "Successfully deleted account"
msgstr "La cuenta se ha eliminado"
